import type { NDKKind } from '@nostr-dev-kit/ndk';
import type { ToastSettings } from '@skeletonlabs/skeleton';
import { defaulRelaysUrls } from '../stores/provider';
export const profileImageUrl = 'https://api.dicebear.com/5.x/identicon/svg?seed=';

// todo: get rid of these kind related variables and instead use the NDKKind enum.
// if a kind number is not present there or we want to refer to a existing kind by a different name,
// create a new object/enum called CustomKind and add kind numbers there.
// ex: we can do something like CustomKind.Opinion = NDKKind.Article

export const kindNotes = 1;
export const kindOpinion = 30023 as NDKKind;
export const kindDelete = 5;
export const kindReaction = 7;
export const kindUpload = 27235;
export const kindRelay = 10002;

export const uploadUrl = 'https://void.cat';
export const toastTimeOut = 3500;
export const nostrBuildBaseApiKey =
	'26d075787d261660682fb9d20dbffa538c708b1eda921d0efa2be95fbef4910a';

export const succesPublishToast: ToastSettings = {
	message: 'Published successfully!',
	timeout: toastTimeOut,
	hoverable: true,
	background: 'variant-filled-success'
};

export const errorPublishToast: ToastSettings = {
	message: 'Error on publishing, look at console!',
	timeout: toastTimeOut,
	hoverable: true,
	background: 'variant-filled-error'
};
export const DEFAULT_RELAY_URLS = {
	read: defaulRelaysUrls,
	write: defaulRelaysUrls
};
export const opinionHeaderSeparator = '\n<!--HEADER END-->\n';
export const opinionFooterSeparator = '\n<!--FOOTER START-->\n\n\n\n';
export const opinionHeaderRegex = new RegExp(`^[\\s\\S]*${opinionHeaderSeparator}`);
export const opinionFooterRegex = new RegExp(`${opinionFooterSeparator}[\\s\\S]*$`);
