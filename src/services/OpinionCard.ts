import { NDKEvent, NDKKind, NDKRelaySet } from '@nostr-dev-kit/ndk';
import BaseCard from '../abstract/BaseCard';
import ndkStore from '../stores/provider';
import { get as getStore } from 'svelte/store';
// eslint-disable-next-line  @typescript-eslint/no-unused-vars
class OpinionCard extends BaseCard {
	constructor(event: NDKEvent) {
		super(event);
	}

	// todo: make editLevel an enum instead of just number
	public async submit(content: string, aTag: string, editLevel: number): Promise<void> {
		const $ndk = getStore(ndkStore);
		const ndkEvent = new NDKEvent($ndk);
		ndkEvent.kind = NDKKind.Text;
		ndkEvent.content = content;
		ndkEvent.tags = [
			['a', aTag],
			['p', this.event.pubkey],
			['e', this.event.id, '', editLevel == 1 ? 'root' : 'reply']
		];
		if (editLevel > 1) {
			this.event.tags.map((e) => {
				if (e[0] === 'p') {
					if (!ndkEvent.tags.toString().includes(e.toString())) {
						ndkEvent.tags.push(e);
					}
				} else if (e[0] === 'e' && e[3] === 'root') {
					ndkEvent.tags.push(e);
				}
			});
		}
		await ndkEvent.publish(NDKRelaySet.fromRelayUrls(super.writeRelays, $ndk));
	}
}
