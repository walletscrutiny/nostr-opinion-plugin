import { NDKEvent, NDKKind, NDKRelaySet } from '@nostr-dev-kit/ndk';
import ndkStore from '../stores/provider';
import { get as getStore } from 'svelte/store';
import { kindRelay } from '../utils/constants';

// Note: This class is not used. But keeping it for now because there are plans to use it in the future
// todo: if not used remove it
export default abstract class BaseCard {
	protected readonly event: NDKEvent;
	private _readRelays: Array<string>;
	private _writeRelays: Array<string>;

	constructor(event: NDKEvent) {
		this.event = event;
		// todo: make these variables sets instead of arrays
		this._readRelays = [];
		this._writeRelays = [];
		this.fetchAuthorsReadAndWriteRelay();
	}

	public async react(reaction: string, aTag: string, editLvl: number): Promise<void> {
		try {
			const $ndk = getStore(ndkStore);
			const ndkEvent = new NDKEvent($ndk);
			ndkEvent.kind = NDKKind.Reaction;
			ndkEvent.content = reaction;
			ndkEvent.tags = [
				['a', aTag],
				['p', this.event.pubkey],
				['e', this.event.id, '', editLvl == 1 ? 'root' : 'reply']
			];
			await ndkEvent.publish(NDKRelaySet.fromRelayUrls(this._writeRelays, $ndk));
		} catch (error) {
			console.error(`Error while reacting to event: ${this.event.id}, Error: ${error}`);
		}
	}

	private async fetchAuthorsReadAndWriteRelay(): Promise<void> {
		const $ndk = getStore(ndkStore);
		const fetchRelays = await $ndk.fetchEvent(
			{ kinds: [kindRelay], authors: [this.event.pubkey] },
			{ closeOnEose: true }
		);
		if (fetchRelays) {
			fetchRelays.getMatchingTags('r').map((tags) => {
				const tagValue = tags[1];
				// todo: add to sets, doing includes and push is resource intensive
				if (tags.length === 3) {
					// todo: instead of magic strings 'read' and 'write', create vars/enum
					if (tags[2] === 'write' && !this._writeRelays.includes(tagValue)) {
						this._writeRelays.push(tagValue);
					} else if (tags[2] === 'read' && !this._readRelays.includes(tagValue)) {
						this._readRelays.push(tagValue);
					}
				} else if (tags.length === 2) {
					if (!this._writeRelays.includes(tagValue)) {
						this._writeRelays.push(tagValue);
					}
				}
			});
		}
	}

	public get writeRelays() {
		return this._writeRelays;
	}

	public get readRelays() {
		return this._readRelays;
	}

	public get getEvent() {
		return this.event;
	}
}
